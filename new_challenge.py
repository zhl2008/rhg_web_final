#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
@name challenge module for RHG-WEB
@author humensec
@version v0.1
@time 2019/03/21

This module is used to interact with the nterface, and download/provide the 
challenge information for the fuzzing module

'''

import os
import sys
import config
import json
import requests
import Queue
from random import randint
import threading
import paramiko
import time
import log
from Scanner import *
from WebExploit import *
from ServerExploit import *

class challenge():

	def __init__(self,mode):
		self.user = config.user
		self.pwd = config.pwd
		self.interface = config.interface
		self.status_url = config.status_url
		self.flag_url = config.flag_url
		self.flag_path = config.flag_path
		self.proxy_start_port = config.proxy_start_port

		self.headers = {"User-Agent":"666666"}
		self.runtime_path = config.runtime_path
		self.exploit_queue = Queue.Queue(1000)
		self.thread_array = []
		self.thread_watch_span = config.thread_watch_span
		self.challenges = {}
		self.mode = mode

		# set pwntools update to never
		if not os.path.exists('/root/.pwntools-cache/'):
			os.mkdir('/root/.pwntools-cache/')

		if not os.path.exists('/root/.pwntools-cache/update'):
			os.system('echo never > /root/.pwntools-cache/update')


		# tell the queue thread the init process has been done
		self.init_fin = 0
		self.get_status()


	def get_status(self):
		'''
			get status(challenges and score) from the interface
		'''

		real_url = self.interface + self.status_url
		
		test = 1
		# loop until there is no error

		while test:
			try:
				r = requests.get(real_url,auth=(self.user,self.pwd),headers=self.headers,verify=False)
				#print r.content
				self.status = json.loads(r.content)
				if not self.status.has_key('AiChallenge'):
					time.sleep(2)
					log.warning('game not start!')
					continue
				test = 0
			except Exception,e:
				test = 1
				log.error(str(e))
		#print r.content

		# prepare the download path
		if not os.path.exists(self.runtime_path):
			os.system('mkdir -p %s' %self.runtime_path)

		self.all_challenges = self.status['AiChallenge']

		

		# put all the challenge to download queue
		for challenge in self.all_challenges:

			challenge_id = challenge["challengeID"]

			# choose the challenge according to the mode: attack/defense
			challenge = challenge[self.mode]
			tmp = {}
			tmp = challenge
			tmp['mode'] = self.mode
			tmp['web_path'] = self.create_folder(self.runtime_path + '/' + self.mode + '_' + str(challenge_id) + '/' + 'web')
			tmp['server_path'] =  self.create_folder(self.runtime_path + '/' + self.mode + '_' + str(challenge_id) + '/' + 'server')
			tmp['challenge_id'] = challenge_id
			tmp['scan_res'] = ''

			# process types: not_start, scan_web, exploit_web, proxy, scan_server, exploit_server, server_flag, done

			if self.mode == 'attack':
				tmp['web_status'] = 'not_start'
				tmp['server_status'] = 'not_start'
				tmp['frp_status'] = 'no'
				tmp['web_scan_res'] = 'None'
				tmp['server_scan_res'] = 'None'
				tmp['web_exp'] = 'None'
				tmp['server_exp'] = 'None'
				tmp['web_success_exp'] = 'None'
				tmp['server_success_exp'] = 'None'
				tmp['web_flag'] = ''
				tmp['server_flag'] = ''
				# using the even port
				tmp['proxy_port'] = self.proxy_start_port + 2
				self.proxy_start_port += 8
				

			elif self.mode == 'defense':

				tmp['web_status'] = 'not_start'
				tmp['server_status'] = 'not_start'
				tmp['web_ssh'] = challenge['web_user'] + ':' + challenge['web_password'] + '@' + challenge['web_ip'] + ':' + str(challenge['web_ssh_port'])
				tmp['server_ssh'] = challenge['server_user'] + ':' + challenge['server_password'] + '@' + challenge['server_ip'] + ':' + str(challenge['server_ssh_port'])

				# using the odd port
				tmp['proxy_port'] = self.proxy_start_port + 6
				self.proxy_start_port += 8

			else:
				log.error('No Such mode!')
				sys.exit()

			self.challenges[challenge_id] = tmp
			self.exploit_queue.put(challenge_id)
		
		log.context(json.dumps(self.challenges))


	def create_folder(self,_dir):
		'''
		create folder with mkdir
		'''
		if not os.path.exists(_dir):
			os.system('mkdir -p %s'%_dir)

		return _dir


	def flag_submit(self,flag):
		
		
		real_url = self.interface + self.flag_url
		data = {"answer":flag}

		# flag count function
		if 'flag' in flag:
			config.flag_count += 1
		if config.flag_count >= config.limit_flag_count:
			log.info("get flag! =>> " + str('flag{overflow}'))
			return True

		log.info("get flag! =>> " + str(flag))

		try:
			r = requests.post(real_url,auth=(self.user,self.pwd),data=data,headers=self.headers,verify=False)
		except Exception,e:
			log.error('error: ' + str(e))
			return 

		self.result = json.loads(r.content)
		#print r.content
		if self.result['status'] == 1:
			log.success('flag correct')
		else:
			 log.error('flag incorrect')
			 log.error(self.result['msg'])

	def thread_watch(self):
		'''
			start a thread to print the status of all alive status
		'''
		while True:

			time.sleep(self.thread_watch_span)

			res = ''
			res += '######  thread status ######\n'
			for my_thread in self.thread_array:
				if my_thread.isAlive():
					res +=  my_thread.name + ' => ' + 'Alive\n' if my_thread.isAlive() else 'Dead\n'
			res += '######  status ends  ######\n'

			if not self.exploit_queue.empty():
				# print self.exploit_queue.qsize()
				all_exp_challenges = str(list(self.exploit_queue.queue))
			else:
				all_exp_challenges = 'empty'
			res += '[+] existing exp queue: %s'%all_exp_challenges

			log.context(res)

			if self.mode == 'attack':
				self.generate_log_attack()
			else:
				self.get_check_info()
				self.generate_log_defense()

			self.heart_beat()


	def hack_server(self,server_id,challenge,port):
		e = ServerExploit(server_id,challenge,port)
		# print server_id,challenge,port
		res = e.flag()
		if res:
			flag = res
			challenge['server_flag'] = flag
			self.flag_submit(flag)
			log.success('Finished hacking the server at port %d: challenge_%d with exp: %s'%(int(port),int(challenge['challenge_id']),server_id))
			return True
		else:
			log.warning('Unable to hack the server at port %d: challenge_%d with exp: %s'%(int(port),int(challenge['challenge_id']),server_id))
		return False

	def read_ssh_key(self,command):
		res = command('cat /var/www/id_rsa')
		if 'PRIVATE KEY' in res:
			open('/tmp/id_rsa','w').write(res)
			log.success('Finding id_rsa!')


	def hack_web(self,web_id,challenge):
		e = WebExploit(web_id,challenge)
		if e.success():
			flag = e.flag()
			challenge['web_flag'] = flag
			self.flag_submit(flag)
			log.success('Finished hacking the web challenge_%d with exp: %s'%(int(challenge['challenge_id']),web_id))
			
			# read the id_rsa
			# self.read_ssh_key(e.command)
			return e
		else:
			# log.error(e.success());
			log.warning('Unable to hack the web challenge_%d with exp: %s'%(int(challenge['challenge_id']),web_id))
		return False


	def generate_log_attack(self):
		col_name = ['ID','Type','Status','Scan_res','Frp','Running_exp','Success_exp','Flag']
		col = {'ID':4,'Type':9,'Status':12,'Scan_res':20,'Frp':4,'Running_exp':15,'Success_exp':15,"Flag":20}
		res = ''

		header =''
		for c in col_name:
			header += c.ljust(col[c],' ')

		res += header + '\n'

		for challenge_id in self.challenges:
			challenge = self.challenges[challenge_id]
			web_line = ''
			web_line += str(challenge_id).ljust(col['ID'],' ')
			web_line += 'WEB'.ljust(col['Type'],' ')
			web_line += challenge['web_status'].ljust(col['Status'],' ')
			web_line += challenge['web_scan_res'].ljust(col['Scan_res'],' ')
			web_line += challenge['frp_status'].ljust(col['Frp'],' ')
			if challenge['web_status'] in  ['Done','Fail','Deploying']:
				challenge['web_exp'] = 'None'
			web_line += challenge['web_exp'].ljust(col['Running_exp'],' ')
			web_line += challenge['web_success_exp'].ljust(col['Success_exp'],' ')
			web_line += challenge['web_flag'].ljust(col['Flag'],' ')
			web_line += '\n'

			server_line = ''
			server_line += str(challenge_id).ljust(col['ID'],' ')
			server_line += 'SERVER'.ljust(col['Type'],' ')
			server_line += challenge['server_status'].ljust(col['Status'],' ')
			server_line += challenge['server_scan_res'].ljust(col['Scan_res'],' ')
			server_line += 'no'.ljust(col['Frp'],' ')
			if challenge['server_status'] in  ['Done','Fail','Deploying']:
				challenge['server_exp'] = 'None'
			server_line += challenge['server_exp'].ljust(col['Running_exp'],' ')
			server_line += challenge['server_success_exp'].ljust(col['Success_exp'],' ')
			server_line += challenge['server_flag'].ljust(col['Flag'],' ')
			server_line += '\n'


			res += web_line + server_line

		log.context('\n' + res + '\n')



	def generate_log_defense(self):
		col_name = ['ID','Type','Status','SSH_info']
		col = {'ID':4,'Type':9,'Status':12,'SSH_info':50}
		res = ''

		header =''
		for c in col_name:
			header += c.ljust(col[c],' ')

		res += header + '\n'

		for challenge_id in self.challenges:
			challenge = self.challenges[challenge_id]
			web_line = ''
			web_line += str(challenge_id).ljust(col['ID'],' ')
			web_line += 'WEB'.ljust(col['Type'],' ')
			web_line += challenge['web_status'].ljust(col['Status'],' ')
			web_line += challenge['web_ssh'].ljust(col['SSH_info'],' ')
			web_line += '\n'

			server_line = ''
			server_line += str(challenge_id).ljust(col['ID'],' ')
			server_line += 'SERVER'.ljust(col['Type'],' ')
			server_line += challenge['server_status'].ljust(col['Status'],' ')
			server_line += challenge['server_ssh'].ljust(col['SSH_info'],' ')
			server_line += '\n'

			res += web_line + server_line

		log.context('\n' + res + '\n')



	def exploit(self):
		'''
			step1: scan the web;
			step2: hack the web;
			step3: submit the web flag;establish the frp (if hack succeed);
			step3: try all exp and hack again( if hack fail);
			step4: scan the server;
			step5: hack the server;
			step6: submit the server flag, done(if hack succeed);
			step6: try all exp and hack again( if hack fail);

			extra notification:
			if web/server_ids is unknown, try all of other exps;
			if fail to hack web/server, try all of other exps
		'''

		challenge_id = self.exploit_queue.get()
		challenge = self.challenges[challenge_id]
		log.info('Handle with challenge_%d'%challenge_id)

		# update status
		challenge['web_status'] = 'Scanning'

		s = Scanner(challenge)
		web_ids = s.WebScan()
		log.info('WebScan result of ' + str(challenge_id) + ':' + str(web_ids))
		challenge['web_scan_res'] = ','.join(web_ids).replace('web_','')

		# update status
		challenge['web_status'] = 'Exploiting'

		web_hack_succ = 0
		web_array = web_ids + [x for x in config.web_exp.values() if x not in web_ids]
		for web_id in web_array:
			# hack the unknown web using all the exps
			if web_id == 'web_unknown':
				continue

			challenge['web_exp'] = web_id
			e = self.hack_web(web_id,challenge)
			if e:
				challenge['web_success_exp'] = web_id
				web_hack_succ = 1

				# update status
				challenge['web_status'] = 'Deploying'

				s.frp_deploy(e.command)
				challenge['frp_status'] = 'ok'

				# update status
				challenge['web_status'] = 'Done'

				# update status
				challenge['server_status'] = 'Scanning'

				scan_res = s.ServerScan()
				log.info('ServerScan result of challenge_%d : %s' %(challenge_id,str(scan_res)))
				challenge['server_scan_res'] = ','.join(scan_res)

				# update status
				challenge['server_status'] = 'Exploiting'

				for port in scan_res:
					server_ids = scan_res[port]
					server_array = [server_ids] + [x for x in config.server_port_exp_map[port] if x not in [server_ids]]
					for server_id in server_array:
						# hack the unknown server using all the exps
						if server_id == 'server_unknown':
							continue
						challenge['server_exp'] = server_id
						if self.hack_server(server_id,challenge,port):
							# update status
							challenge['server_status'] = 'Done'
							challenge['server_success_exp'] = server_id
							return True
				# web hack done		
				break
		
		log.error('Fail to hack the server_%d'%int(challenge['challenge_id']))
		# update status
		challenge['server_status'] = 'Fail'
		if not web_hack_succ:
			log.error('Fail to hack the web_%d'%int(challenge['challenge_id']))
			# update status
			challenge['web_status'] = 'Fail'
			challenge['server_status'] = 'Fail'


	def exploit_control(self):
		# if the exploit queue is empty, it means all the tasks have been done
		while True:
			if self.exploit_queue.empty():
				log.success('Empty queue, almostly finished! Thread quitting...')
				return
			else:
				if self.mode == 'attack':
					self.exploit()
				else:
					self.defense()

			time.sleep(config.timeout)
		

						

	def defense(self):
	
		'''
			for web challenge:
			run the web_repair to install the openrasp

			for server challenge:
			run the repair script of python

			check the status every 10s
		'''

		challenge_id = self.exploit_queue.get()
		challenge = self.challenges[challenge_id]

		challenge['web_status'] = 'Handling'
		self.repair_web(challenge)
		challenge['web_status'] = 'Finished'

		challenge['server_status'] = 'Handling'
		self.repair_server(challenge)
		challenge['server_status'] = 'Finished'


	def check_status(self,_id):
		try:
			data = {"ChallengeID":_id}
			real_url = self.interface + config.check_url
			r = requests.post(real_url,auth=(self.user,self.pwd),headers=self.headers,verify=False,data=data)
		except Exception,e:
			log.error('error: ' + str(e))
			return 

	def heart_beat(self):
		try:
			real_url = self.interface + config.heartbeat_url
			r = requests.get(real_url,verify=False,auth=(self.user,self.pwd))
		except Exception,e:
			log.error('error: ' + str(e))
			return



	def check_control(self):
		while True:
			for challenge_id in self.challenges:
				self.check_status(challenge_id)
			time.sleep(70)
			log.success('check request has been sent')


	def get_check_info(self):
		'''
			get the check info in the defense mode

		'''
		try:
			real_url = self.interface + config.check_info_url
			r = requests.get(real_url,auth=(self.user,self.pwd),headers=self.headers,verify=False)
		except Exception,e:
			log.error('error: ' + str(e))
			return 
		# print r.content
		self.result = json.loads(r.content)

		check_status = self.result['check_status']

		for c in check_status:
			challenge_id = c['challengeID']
			# update the status only the repairment is finished
			if self.challenges[challenge_id]['web_status'] == 'Finished':
				self.challenges[challenge_id]['web_status'] = str(c['web'])
			if self.challenges[challenge_id]['server_status'] == 'Finished':
				self.challenges[challenge_id]['server_status'] = str(c['server'])


	def repair_web(self,challenge):
		_id = challenge['challenge_id']
		ip = challenge['web_ip']
		port = challenge['web_ssh_port']
		username = challenge['web_user']
		password = challenge['web_password']

		cmd = self.install_rasp()

		cmd += ';'
		cmd += 'find / -name ".git" | xargs rm -rf;'
		cmd += 'find / -name ".svn" | xargs rm -rf;'
		cmd += 'find / -name "id_rsa"| xargs rm -rf;'
		# repair web_14
		cmd += 'mv /var/www/html/core/modules/hal/src/LinkManager/TypeLinkManager.php /var/www/html/core/modules/hal/src/LinkManager/TypeLinkManager.php.bak'
		#cmd += self.repair_weak_pass()

		log.context(cmd)

		self.my_ssh_batch(ip,port,username,password,cmd)
		log.success('repair web_%s done' %(str(_id)))

	def repair_server(self, challenge):
		_id = challenge['challenge_id']
		ip = challenge['server_ip']
		port = challenge['server_ssh_port']
		username = challenge['server_user']
		password = challenge['server_password']

		if not 'Wget' in self.my_ssh_single(ip, port, username, password, 'wget -V'):
			log.info('uploading wget binary...')
			self.scp(ip, port, username, password, './download_web/wget', '/bin/wget')
			self.my_ssh_single(ip, port, username, password, 'chmod +x /bin/wget')

		cmd = ''
		if 'elasticsearch ' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep elasticsearch | grep -v "grep"'):
			cmd += self.repair_elasticsearch() + ';'

		if 'weblogic' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep weblogic | grep -v "grep"'):
			cmd += self.repair_weblogic() + ';'
		# try:
		# 	jenkins_version = requests.get(url='http://' + ip + ':8080').headers['X-Jenkins']
		# 	if jenkins_version == '2.176':
		# 		cmd += self.repair_jenkins176() + ';'
		# 	elif jenkins_version == '2.46.1':
		# 		cmd += self.repair_jenkins461_1() + ';'  # 由于第一部分的 wget下载部分比较大 容易造成还没下载完就执行下一条命令
		# 		cmd += self.repair_jenkins461_2() + ';'  # 需要中间有个停顿 不知道在这怎么实现
		# except:
		# 	pass

		if 'jenkins' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep jenkins| grep -v "grep"'):
			print "------------------------------------------------------------------"
			cmd += self.repair_jenkinsall() + ';'
		# elif '2.46.1' in self.my_ssh_single(ip, port, username, password, 'curl http://127.0.0.1:8080'):
		# 	cmd += self.repair_jenkins461_1() + ' && '  # 由于第一部分的 wget下载部分比较大 容易造成还没下载完就执行下一条命令
		# 	cmd += self.repair_jenkins461_2() + ';'  # 需要中间有个停顿 不知道在这怎么实现

		if 'zookeeper' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep zookeeper | grep -v "grep"'):
			cmd += self.repair_zookeeper() + ';'  # 这个不知为什么实验不成功，exp也看不明白

		if 'struts2-struts1-plugin' in self.my_ssh_single(ip, port, username, password,
														  'find / -name "struts2-struts1-plugin*"'):
			cmd += self.repair_struts2() + ';'

		if '5.7.26' in self.my_ssh_single(ip, port, username, password, 'mysql -V'):
			cmd += self.repair_mysql_1() + ';'

		if 'mysql' in self.my_ssh_single(ip, port, username, password, 'mysql -V'):
			cmd += self.repair_mysql_2() + ';'

		if 'tomcat' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep tomcat | grep -v "grep"'):
			cmd += self.repair_tomcat() + ';'

		if 'nagios' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep nagios | grep -v "grep"'):
			cmd += self.repair_nagios() + ';'

		if 'apache' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep apache | grep -v "grep"'):
			cmd += self.install_rasp() + ';'  


		if 'spring' in self.my_ssh_single(ip, port, username, password, 'ps -ef | grep spring | grep -v "grep"'):
			cmd += self.repair_spring() + ';'  # 这个函数没写

		cmd = cmd[:-1]
		log.context(cmd)
		log.warning(self.my_ssh_batch(ip, port, username, password, cmd))

		log.success('repair server_%s done' % (str(_id)))


	def repair_elasticsearch(self):  # server1
		cmd = ''
		cmd += 'wget %s/elasticsearch/elasticsearch-1.4.3.tar.gz -O /tmp/elasticsearch-1.4.3.tar.gz;' % config.down_path
		cmd += 'cd /tmp;'
		cmd += 'tar xvfz /tmp/elasticsearch-1.4.3.tar.gz;'
		cmd += 'pidof java | xargs kill -9;'
		cmd += 'sh elasticsearch-1.4.3/bin/elasticsearch -d'

		return cmd

	def repair_mysql_2(self):
		cmd = ''
		cmd += '''sed -i 's/^secure_file_priv=.*//g' /etc/mysql/my.cnf;'''
		cmd += 'service mysql stop;'
		cmd += 'service mysql start'
		return cmd


	def repair_weblogic(self):  # server2
		cmd = ''
		cmd += 'rm - rf $(find / -name wls-wsat.war);'
		cmd += 'pidof java | xargs kill -9;'
		cmd += 'find / -name startWebLogic.sh;'
		cmd += 'sh $(find / -name startWebLogic.sh | grep base_domain | grep bin)'

		return cmd


	def repair_jenkins176(self):  # server3
		cmd = ''
		cmd += 'cd $(find /var -name "config.xml*"| head -n 1 | xargs dirname );'
		cmd += 'mv config.xml config.xml.bak;'
		cmd += 'wget %s/jenkins176/config.xml .' % config.down_path

		return cmd
	def repair_jenkinsall(self):  # server3,6
		cmd = ''
		cmd += 'cd $(find /var -name "config.xml*"| head -n 1 | xargs dirname );'
		cmd += 'mv config.xml config.xml.bak&&'
		cmd += 'wget %s/jenkins176/config.xml&&' % config.down_path
		cmd += 'cd $(find /usr -name "jenkins.war*"| head -n 1 | xargs dirname )&&'
		cmd += 'mv jenkins.war jenkins.war.bak&&'
		cmd += 'wget %s/jenkins.war&&' % config.down_path
		cmd += 'chmod 777 jenkins.war&&'
		cmd += '''ps -ef|grep 'jenkins'|grep -v 'ii'|cut -c 9-15|xargs kill -9;'''
		cmd += 'java -Duser.home=/var/jenkins_home -Djenkins.install.runSetupWizard=false -jar jenkins.war > /dev/null 2>&1 &'


		return cmd


	def repair_zookeeper(self):  # server4
		cmd = ''
		cmd += 'export PATH=/opt/bitnami/java/bin:/opt/bitnami/zookeeper/bin:/opt/bitnami/nami/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin;'
		# cmd += 'zkCli.sh -server localhost:2181;'
		# cmd += 'addauth digest user:password;'
		# cmd += 'setAcl /tmp/flag auth:user:password:cdrwa;'
		# cmd += 'quit'
		cmd += '/opt/bitnami/zookeeper/bin/zkCli.sh -server 127.0.0.1:2181 delete /tmp/flag;'

		return cmd


	def repair_struts2(self):  # server5
		cmd = ''
		cmd += 'cd $(find / -name "WEB-INF" | xargs dirname );'
		cmd += 'rm WEB-INF/lib/struts2-struts1-plugin*;'
		cmd += 'cd ../../bin;'
		cmd += 'sh shutdown.sh;'
		cmd += 'sh startup.sh'

		return cmd


	def repair_jenkins461_1(self):  # server6-1
		cmd = ''
		cmd += '''ps -ef|grep 'jenkins'|grep -v 'ii'|cut -c 9-15|xargs kill -9;'''
		cmd += 'cd $(find /usr -name "jenkins.war*"| head -n 1 | xargs dirname );'
		cmd += 'mv jenkins.war jenkins.war.bak;'
		cmd += 'wget %s/jenkins.war .' % config.down_path

		return cmd


	def repair_jenkins461_2(self):  # server6-2
		cmd = ''
		cmd += 'cd $(find /usr -name "jenkins.war*"| head -n 1 | xargs dirname );'
		cmd += 'java -Duser.home=/var/jenkins_home -Djenkins.install.runSetupWizard=false -jar jenkins.war &'

		return cmd


	def repair_mysql_1(self):  # server7
		cmd = ''
		cmd += 'cd /usr/lib/mysql;'
		cmd += 'chmod -R 755 plugin'

		return cmd


	def repair_tomcat(self):  # server8
		cmd = ''
		cmd += 'cd /usr/local/tomcat/conf;'
		cmd += 'mv web.xml web.xml.bak;'
		cmd += 'wget %s/tomcat/web.xml .' % config.down_path

		return cmd


	def repair_nagios(self):  # server9
		cmd = ''
		cmd += "rm -rf /usr/local/nagios/share/includes/*;"
		cmd += "find / -name 'rss-corefeed.php' | xargs rm -rf;"
		cmd += "find / -name 'rss-newsfeed.php' | xargs rm -rf;"
		cmd += "find / -name 'rss-corebanner.php' | xargs rm -rf"

		return cmd


	def repair_spring(self): # server10 没写
		cmd = ''
		cmd += 'killall java;'
		cmd += 'wget %s/spring/spring.jar -O /tmp/spring.jar;' % config.down_path
		cmd += 'java -jar /tmp/spring.jar &'
		return cmd




	def install_rasp(self):  # 通防
		cmd = ''
		cmd += 'wget %s/rasp.tgz -O /tmp/rasp.tgz;' % config.down_path
		cmd += 'cd /tmp;'
		cmd += 'tar xvfz /tmp/rasp.tgz;'
		cmd += 'cd /tmp/rasp;'
		cmd += 'php install.php -d /opt/rasp --url %s;' % config.down_path
		cmd += 'cp /tmp/rasp/test.php /var/www/html/testcmd.php;'
		cmd += 'service apache2 restart'

		return cmd

	def my_ssh_single(self,ip,port,username,password,cmd):

		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		try:
			ssh.connect(ip, port, username, password, timeout=4)
			stdin, stdout, stderr = ssh.exec_command(cmd,timeout=4)
			res = stdout.readlines()
			if res:
				return res[0].strip()
			else:
				return 'error'

		except Exception,e:
			log.error(str(e))
			return 'error'



	def my_ssh_batch(self,ip,port,username,password,cmd):

		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		try:
			ssh.connect(ip, port, username, password, timeout=4)
			# stdin, stdout, stderr = ssh.exec_command(cmd,timeout=400)
			# res = stdout.readlines()[0].strip()

			channel = ssh.invoke_shell()
			stdin = channel.makefile('wb')
			stdout = channel.makefile('rb')

			stdin.write(cmd+'\nexit\n')
			res = stdout.read()
			stdout.close()
			stdin.close()
			ssh.close()

			return res

		except Exception,e:
			log.error(str(e))
			return 'error'



	def scp(self,ip,port,username,password,file_src,file_dest):
		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		
		try:
			ssh.connect(ip, port, username, password, timeout=4)
			sftp = ssh.open_sftp()
			res = sftp.put(file_src,file_dest)
			if res:
				return True
			else:
				return False
			
		except Exception,e:
			log.error(str(e))
			return False

	def destory(self):
		# delete all the source code when the game is over.
		pass




		

if __name__ == '__main__':


	# enable attack mode
	c = challenge(sys.argv[1])

	# enable defense mode
	# c = challenge('defense')

	t = threading.Thread(target=c.thread_watch,name='watch_dog')
	log.info('one thread for watchdog has been created!')
	c.thread_array.append(t)

	if c.mode == 'defense':
		t= threading.Thread(target=c.check_control,name='check_thread')
		log.info('one thread for check_thread has been created!')
		c.thread_array.append(t)

	for i in range(config.exploit_thread):
		t = threading.Thread(target=c.exploit_control,name='exploit_thread_%d'%(i+1))	
		log.info('one thread for attacking/defensing has been created!')
		c.thread_array.append(t)
	

	for t in c.thread_array:
		t.setDaemon(True)
		t.start()


	try:
		while 1:
			# print 'main thread hello'
			time.sleep(5)
			# print 'main thread alive'
	except KeyboardInterrupt:
		log.error("Program stoped by user, existing...")
		sys.exit()