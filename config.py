#!/usr/bin/env python

'''
	configuration for the game

'''

# debug mode
debug = False

# username for the challenge interface
#user = 'user13'
user = 'team06'
user = 'admin'

# password for the challenge interface
#pwd = '524103'
pwd = '5ACPJ54p'
pwd = '123456'

# url for the interface
interface = 'https://rhg.ichunqiu.com/rhg'
interface = 'http://39.97.182.15:5000/rhg'
#interface = 'https://172.16.4.1'

# interface for getting the status
status_url = '/api/get_question_status'

# interface for submitting the flag
flag_url = '/api/sub_answer'

# interface for resetting
reset_url = '/api/reset_question'

# interface for chekcing 
check_url = '/api/call_question_check'

# flag path
flag_path = '/tmp/flag'

# interface for getting check info
check_info_url = '/api/get_check_info'

# interface for alive detect
heartbeat_url = '/api/heartbeat' 

# runtime path
runtime_path = '/tmp'

# tool path (portscan and proxy)
tool_path = './tool'

# log file
log_file = 'my_log'

# whether write logs to file
log_to_file = 1

# attack/defense thread num
exploit_thread = 5	

# thread watch time span
thread_watch_span = 10

# proxy start port
proxy_start_port = 18800

# port_list to be scanned
port_list = [80,2181,3306,7001,8080,9200]

# timeout for http connection and sockets
timeout = 8

# ip of myself
self_ip = '39.97.182.15'

# port of myself
self_port = 8888


# config download path of frp
frp_down_path = 'http://%s:%s/frp' %(self_ip, str(self_port))

down_path = 'http://%s:%s' %(self_ip, str(self_port))

# the web exp list
web_exp = {
	"joomla_3.4.5":"web_11",
	"Discuz! X3.4":"web_12",
	"thinkphp_5.0.20":"web_13",
	"drupal_8.6.9":"web_14",
	"drupal_7.3":"web_15",
	"drupal_8.5.0":"web_16",
	"DedeCMS V5.7-UTF8-SP2_1":"web_17",
	"DedeCMS V57_GBK_SP1":"web_18",
	"DedeCMS V5.7-UTF8-SP2_2":"web_19",
	"DedeCMS V5.7-UTF8-SP2_3":"web_20"
}

# the server exp list
server_exp = {
	
    "elasticsearch":"server_11",
    "weblogic_10.3.6.0":"server_12",
    "Jenkins_2.176":"server_13",
    "zookeeper_3.4.14":"server_14",
    "struts2_2.3.32":"server_15",
    "Jenkins_2.46.1":"server_16",
    "mysql_5.7.25":"server_17",
    "tomcat_8.5.19":"server_18",
    "Nagios-core_4.2.0":"server_19",
    "spring-data-common 2.0.5":"server_20",
    "web_11":"server_1",
	"web_12":"server_2",
	"web_13":"server_3",
	"web_14":"server_4",
	"web_15":"server_5",
	"web_16":"server_6",
	"web_17":"server_7",
	"web_18":"server_8",
	"web_19":"server_9",
	"web_20":"server_10"
}


# server port mapping
# scan_array = scan_array = [self.elasticsearch,self.weblogic,self.jenkins,self.mysql,self.zookeeper,self.tomcat,self.nagios,self.spring,self.web]

server_port_map = {
	"80": [6,8],
	"2181": [4],
	"3306": [3],
	"8080": [2,5,7],
	"7001": [1],
	"9200": [0]
}

server_port_exp_map = {
	"80": ['server_19','server_1','server_2','server_3','server_4','server_5','server_6','server_7','server_8','server_9','server_10'],
	"2181": ['server_14'],
	"3306": ['server_14','server_17'],
	"7001": ['server_12'],
	"8080": ['server_13','server_14','server_15','server_16','server_20'],
	"9200": ['server_11']
}


flag_count = 0
limit_flag_count = 100






