#!/usr/bin/env python
#coding=utf-8
import sys
print sys.getdefaultencoding()
from flask import Flask, jsonify,request
from flask_basicauth import BasicAuth

app = Flask(__name__)
app.config['BASIC_AUTH_USERNAME'] = 'admin'
app.config['BASIC_AUTH_PASSWORD'] = '123456'
app.config['BASIC_AUTH_FORCE'] = True
basic_auth = BasicAuth(app)
app.config['JSON_AS_ASCII'] = False
@app.route('/rhg/api/get_question_status')
@basic_auth.required
def getStatus():
    AiChallenge = {
        "AiChallenge":[{
            "challengeID":1,
            "attack":{
                "web_ip":"39.97.238.239",
                "web_port":8001,
                "server_ip":"172.16.10.3"
            },
            "defense":{
                "web_ip":"39.97.238.239",
                "web_port":80,
                "web_ssh_port":2201,
                "web_user":"root",
                "web_password":"root",
                "server_ip":"39.97.238.239",
                "server_ssh_port":2301,
                "server_user":"root",
                "server_password":"root"
            }
        },
        {
            "challengeID":2,#题目id
            "attack":{
                "web_port":8002,#web题目访问端口
                "server_ip":"172.16.20.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2202,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2302,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":3,#题目id
            "attack":{
                "web_port":8003,#web题目访问端口
                "server_ip":"172.16.30.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2203,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2303,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":4,#题目id
            "attack":{
                "web_port":8004,#web题目访问端口
                "server_ip":"172.16.40.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2204,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2304,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":5,#题目id
            "attack":{
                "web_port":8005,#web题目访问端口
                "server_ip":"172.16.50.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2205,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2305,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":6,#题目id
            "attack":{
                "web_port":8006,#web题目访问端口
                "server_ip":"172.16.60.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2206,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2306,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":7,#题目id
            "attack":{
                "web_port":8007,#web题目访问端口
                "server_ip":"172.16.70.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2207,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2307,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":8,#题目id
            "attack":{
                "web_port":8008,#web题目访问端口
                "server_ip":"172.16.80.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2208,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2308,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":9,#题目id
            "attack":{
                "web_port":8009,#web题目访问端口
                "server_ip":"172.16.90.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2209,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2309,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        },
        {
            "challengeID":10,#题目id
            "attack":{
                "web_port":8010,#web题目访问端口
                "server_ip":"172.16.100.3",#server题目访问ip
                "web_ip":"39.97.238.239"#web题目访问ip
            },
            "defense":{
                "web_ip":"39.97.238.239",#web题目访问ip
                "web_port":80,#web题目访问端口
                "web_ssh_port":2210,#web docker 访问端口
                "web_user":"root",#web docker 用户名
                "web_password":"root",#web docker 密码
                "server_ip":"39.97.238.239",#server题目访问ip
                "server_ssh_port":2310,#server docker 访问端口
                "server_user":"root",#server docker 用户名
                "server_password":"root",#server docker 密码
            }
        }


        ],
        "status":1,
        "current_time":1551767488
    }
        

    return jsonify(AiChallenge)

@app.route('/rhg/api/sub_answer',methods = ['POST', 'GET'])
@basic_auth.required
def subAnswer():
    status1 = {
        "status":1,
        "msg":"success"
    }
    status2 = {
        "status":0,
        "msg":u"error:post data is empty"
    }  
    try:
        answer = request.form['answer']
        if answer=="":
            return jsonify(status2)
        return jsonify(status1)

    except:
        return jsonify(status2)

@app.route('/rhg/api/reset_question',methods = ['POST', 'GET'])
@basic_auth.required
def resetQuestion():
    status1 = {"status":1,"msg":"success:Please wait for 30s"}
    status2 = {"status":0,"msg":"提示信息"}
    try:
        id = request.form['ChallengeID']
        ttype = request.form['type']
        if id=="":
            return jsonify(status2)
        return jsonify(status1)
    except:
        return jsonify(status2)

@app.route('/rhg/api/get_check_info')
@basic_auth.required
def checkInfo():
    return '''
    {
    "status": 1,
    "check_status": [{
        "challengeID": 1, 
        "web": [1, 0],  
        "server": [1, 0]
    },{
        "challengeID": 2,
        "web": [1, 0],
        "server": [1, 0]
    }]
    }
    '''

@app.route('/rhg/api/call_question_check',methods = ['POST', 'GET'])
@basic_auth.required
def QuestionCheck():
    status1 = {
        "status":1,
        "msg":"success"
    }
    status2 = {
        "status":0,
        "msg":u"error:post data is empty"
    }
    try:
        id = request.form['ChallengeID']
        if id== "":
            return jsonify(status2)
        return jsonify(status1)
    except:
        return jsonify(status2)
        
 
if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0")
