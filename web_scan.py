#!/usr/bin/env python
# -*- coding -*- :utf8

import requests
import re
import socket,socks
# import nmap
import sys
import config
import log
import os
from urllib import quote
import time
import eventlet
from eventlet.timeout import Timeout
import json

# patch for the timeout
# eventlet.monkey_patch()

class WebScan():

	def __init__(self,proxy_port,ip,web_port):
		self.proxy_port = proxy_port
		self.ip = ip
		self.web_port = web_port
		self.timeout = config.timeout
		self.web_exp = config.web_exp

		# set the global socket
		# socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		# socket.socket = socks.socksocket


	def scan(self):
		'''
			1. If the port for each service is fixed, then traverse the port to determine which kind of scanner should be used;
			2. If the port for each service is flexible, then traverse the port with whole the payload going to be used;

		'''

		scan_array = [self.dedecms,self.drupal,self.joomla,self.discuz,self.thinkphp]
		self.web_id = {}


		for port in self.web_port:
			for scan_func in scan_array:

				with Timeout(config.timeout,False):
					tmp_res = scan_func(port)
				
				if tmp_res != -1:
					self.web_id[str(port)] = tmp_res
					break

			if not self.web_id.has_key(str(port)):
				self.web_id[str(port)] = 'web_unknown'

		return json.dumps(self.web_id)


	def joomla(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"+"administrator/manifests/files/joomla.xml"
		#print url
		try:
			result = requests.get(url,timeout = self.timeout)
			if result.status_code == 200:
				joomla_version  = re.compile(r'(?<=<version>)([\s\S]*?)(?=</version>)').findall(result.content)
				if joomla_version[0] == '3.4.5':
					return self.web_exp['joomla_3.4.5']
		except Exception,e:
			if config.debug:
				print e
		return -1
		

	def discuz(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			#method1 "/static/js/admincp.js"
			content = requests.get(url+"static/js/admincp.js",timeout = self.timeout).content
			if content.find("Discuz")!=-1:
				return self.web_exp['Discuz! X3.4']
			#method2 "/forum.php"
			content = requests.get(url+"forum.php",timeout = self.timeout).content
			if content.find("www.discuz.net")!=-1 or content.find("Discuz")!=-1:
				return self.web_exp['Discuz! X3.4']
		except Exception,e:
			if config.debug:
				print e
		return -1


	def thinkphp(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			#method1 
			content = requests.get(url,timeout = self.timeout).content
			if content.find("ThinkPHP")!=-1:
				return self.web_exp['thinkphp_5.0.20']
		except Exception,e:
			if config.debug:
				print e
		return -1


	def drupal(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			#8.X
			result = requests.get(url+"core/themes/bartik/bartik.info.yml",timeout = self.timeout)
			if result.status_code == 200:
				drupal_version  = re.compile(r'(?<=version: \')([\s\S]*?)(?=\')').findall(result.content)
				if drupal_version[0]=="8.6.9":
					return self.web_exp['drupal_8.6.9']
				elif drupal_version[0]=="8.5.0":
					return self.web_exp['drupal_8.5.0']
			#7.X
			result2 = requests.get(url+"modules/help/help.info",timeout = self.timeout)
			if result2.status_code == 200: 
				drupal_version  = re.compile(r'(?<=version = \")([\s\S]*?)(?=\")').findall(result2.content)
				if drupal_version[0]=="7.3":
					return self.web_exp['drupal_7.3']
		except Exception,e:
			if config.debug:
				print e
		return -1


	def dedecms(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			#method1 plus/search.php?kwtype=0&q=233&searchtype=title
			content = requests.get(url+"plus/search.php?kwtype=0&q=233&searchtype=title",timeout = self.timeout).content
			if content.find("DedeCMSV57_UTF8_SP2")!=-1:
				return self.web_exp['DedeCMS V5.7-UTF8-SP2_1']+','+self.web_exp['DedeCMS V5.7-UTF8-SP2_2']+','+self.web_exp['DedeCMS V5.7-UTF8-SP2_3']
			elif content.find("DedeCMSV57_GBK_SP1")!=-1:
				return self.web_exp['DedeCMS V57_GBK_SP1']
		except Exception,e:
			if config.debug:
				print e
		return -1


if __name__ == '__main__':
	# set the debug to 0 to aviod the interference from the output
	config.debug = 0
	if len(sys.argv) == 4:
		s = WebScan(int(sys.argv[1]),sys.argv[2],map(int,sys.argv[3].split(',')))
		print s.scan()
	else:
		s = WebScan(6666,'rhg4',[8001])
		print s.scan()
		s = WebScan(6666,'rhg4',[8002])
		print s.scan()
		s = WebScan(6666,'rhg4',[8003])
		print s.scan()
		s = WebScan(6666,'rhg4',[8004])
		print s.scan()
		s = WebScan(6666,'rhg4',[8005])
		print s.scan()
		s = WebScan(6666,'rhg4',[8006])
		print s.scan()
		s = WebScan(6666,'rhg4',[8007])
		print s.scan()
		s = WebScan(6666,'rhg4',[8008])
		print s.scan()
		s = WebScan(6666,'rhg4',[8009])
		print s.scan()
		s = WebScan(6666,'rhg4',[8010])
		print s.scan()
	
