#!/usr/bin/env python
# -*- coding -*- :utf8

import requests
import re
import socket,socks
# import nmap
import sys
import config
import log
import os
from urllib import quote
import time
import eventlet
from eventlet.timeout import Timeout
import json
import web_scan

# patch for the timeout
# eventlet.monkey_patch()

class ServerScan():

	def __init__(self,proxy_port,ip,server_port):
		self.proxy_port = proxy_port
		self.ip = ip
		self.server_port = server_port
		self.timeout = config.timeout
		self.server_exp = config.server_exp

		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket


	def scan(self):
		'''
			1. If the port for each service is fixed, then traverse the port to determine which kind of scanner should be used;
			2. If the port for each service is flexible, then traverse the port with whole the payload going to be used;

		'''

		scan_array = [self.elasticsearch,self.weblogic,self.jenkins,self.mysql,self.zookeeper,self.tomcat,self.nagios,self.spring,self.web]
		self.server_id = {}


		for port in self.server_port:
			scan_func_indexes = config.server_port_map[str(port)]
			for scan_func_index in scan_func_indexes:
				scan_func = scan_array[scan_func_index]

				with Timeout(config.timeout,False):
					tmp_res = scan_func(port)
				
				if tmp_res != -1:
					self.server_id[str(port)] = tmp_res
					break

			if not self.server_id.has_key(str(port)):
				self.server_id[str(port)] = 'server_unknown'

		return json.dumps(self.server_id)


	def elasticsearch(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			#method1 
			content = requests.get(url,timeout = self.timeout).content
			try:
				elasticsearch_info = re.compile(r'(?<=cluster_name)([\s\S]*?)(?=",)').findall(content)
				if elasticsearch_info[0].find("elasticsearch")!=-1:
					return self.server_exp['elasticsearch']
			except:
				pass
			#method2 "/website/blog/"
			code  = requests.get(url+"website/blog/",timeout = self.timeout).status_code
			if code == 400:
				return self.server_exp['elasticsearch']
		except Exception,e:
			if config.debug:
				print e
		return -1
		

	def weblogic(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			#method1 "console/login/LoginForm.jsp"
			result = requests.get(url+"console/login/LoginForm.jsp",timeout = self.timeout)
			if result.status_code == 200:
				if result.content.find("WebLogic Server")!=-1:
					return self.server_exp['weblogic_10.3.6.0']

		except Exception,e:
			if config.debug:
				print e
		return -1


	def jenkins(self,port):
		url = "http://"+ self.ip + ":" + str(port) +"/"
		try:
			#method1 
			content = requests.get(url,timeout = self.timeout).content
			if content.find("Jenkins")!=-1:
				jenkins_version = re.compile(r'(?<=Jenkins ver. )([\s\S]*?)(?=</a>)').findall(content)
				if jenkins_version[0] == "2.176":
					return self.server_exp['Jenkins_2.176']
				elif jenkins_version[0] == "2.46.1":
					return self.server_exp['Jenkins_2.46.1']
		except Exception,e:
			if config.debug:
				print e
		return -1


	def mysql(self,port):
		try:
			client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			client.settimeout(self.timeout)
			client.connect((self.ip,int(port)))
			client.send("test")
			response = client.recv(4096)
			if response.find("mysql")!=-1:
				return self.server_exp['mysql_5.7.25']
			else:
				return -1
		except Exception,e:
			if config.debug:
				print e
		return -1


	def zookeeper(self,port):
		try:
			client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			client.settimeout(self.timeout)
			client.connect((self.ip,int(port)))
			client.send("test")
			response = client.recv(4096)
			if response.find("mysql")!=-1:
				return self.server_exp['mysql']
			else:
				return -1
		except Exception,e:
			if config.debug:
				print e
		return -1


	def tomcat(self,port):
		url = "http://"+ self.ip + ":" + str(port) + "/"
		try:
			if requests.get(url+"manager/html",timeout=self.timeout * 1000).status_code == 401:
				return self.server_exp['tomcat_8.5.19']
			elif requests.get(url+"manager/text",timeout=self.timeout * 1000).status_code == 401:
				return self.server_exp['tomcat_8.5.19']
			else:
				content = requests.get(url,timeout=self.timeout * 1000).content
				if content.find("Apache Software Foundation")!=-1:
					return self.server_exp['tomcat_8.5.19']
		except Exception,e:
			if config.debug:
				print e
		return -1

	def nagios(self,port):
		url = "http://"+ self.ip + ":" + str(port) + "/"
		try:
			code = requests.get(url+"nagios",timeout=self.timeout * 1000).status_code
			if code == 401:
				return self.server_exp['Nagios-core_4.2.0']
		except Exception,e:
			if config.debug:
				print e
		return -1

	def spring(self,port):
		url = "http://"+ self.ip + ":" + str(port) + "/"
		try:
			content = requests.get(url+"users?page=&size=",timeout=self.timeout * 1000).content
			if content.find("spring")!=-1:
				return self.server_exp['spring-data-common 2.0.5']
		except Exception,e:
			if config.debug:
				print e
		return -1

	def web(self,port):
		# patch for web
		s = web_scan.WebScan(6666,self.ip,["80"])
		res = []
		tmp =  json.loads(s.scan())['80'].split(',')
		res = self.server_exp[tmp[0]]

		return res



if __name__ == '__main__':
	# set the debug to 0 to aviod the interference from the output
	config.debug = 0
	if len(sys.argv) == 4:
		s = ServerScan(int(sys.argv[1]),sys.argv[2],map(int,sys.argv[3].split(',')))
		print s.scan()
	
