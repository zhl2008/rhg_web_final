#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import sys
import config
import socks,socket
import log
import time

ip = sys.argv[1]
proxy_ip = '127.0.0.1'
proxy_port = int(sys.argv[2])
res = ''

socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",proxy_port)

for port in config.port_list:
	try:
		# using the socks-proxy to scan the server
		if config.debug:
			log.warning('Sleep 1 sec before scanning port %d...'%port)
		time.sleep(1)
		server = socks.socksocket(socket.AF_INET, socket.SOCK_STREAM)
		server.settimeout(config.timeout)
		server.connect((ip,port))
		server.close()
		res += str(port) + ','

	except Exception,e:
		if config.debug:
			print e
		server.close()

print res[:-1]
