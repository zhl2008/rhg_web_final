#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import config

def black(string):
	return'\033[30m'+string+'\033[0m'

def blue(string):
	return'\033[94m'+string+'\033[0m'

def gray(string):
	return'\033[1;30m'+string+'\033[0m'

def green(string):
	return'\033[92m'+string+'\033[0m'

def cyan(string):
	return'\033[96m'+string+'\033[0m'

def lightPurple(string):
	return'\033[94m'+string+'\033[0m'

def purple(string):
	return'\033[95m'+string+'\033[0m'

def red(string):
	return'\033[91m'+string+'\033[0m'

def underline(string):
	return'\033[4m'+string+'\033[0m'

def white(string):
	return'\033[0m'+string+'\033[0m'

def white_2(string):
	return'\033[1m'+string+'\033[0m'

def yellow(string):
	return'\033[93m'+string+'\033[0m'

def _print(word):
	sys.stdout.write(word)
	sys.stdout.flush()

def write_log(word):
	if config.log_to_file:
		open(config.log_file,'a').write(word)

def info(word):
	write_log("[*] %s\n" % word)
	_print("[*] %s\n" % green(word))


def warning(word):
	write_log("[!] %s\n" % word)
	_print("[!] %s\n" % yellow(word))


def error(word):
	write_log("[-] %s\n" % word)
	_print("[-] %s\n" % red(word))


def success(word):
	write_log("[+] %s\n" % word)
	_print("[+] %s\n" % purple(word))

	
def context(word):
	write_log("%s\n" % word)
	_print("%s\n" % blue(word))


# test
if __name__ == '__main__':
	info('humensec')
	warning('humensec')
	error('humensec')
	success('humensec')
	context('humensec')


