#!/usr/bin/env python


# the ip to be attacked
default_target_ip = 'rhg4'

ports_mapping = {
'web_11.py': '8001',
'web_12.py': '8002',
'web_13.py': '8003',
'web_14.py': '8004',
'web_15.py': '8005',
'web_16.py': '8006',
'web_17.py': '8007',
'web_18.py': '8008',
'web_19.py': '8009',
'web_20.py': '8010',
'server_1.py':'9001',
'server_2.py':'9002',
'server_3.py':'9003',
'server_4.py':'9004',
'server_5.py':'9005',
'server_6.py':'9006',
'server_7.py':'9007',
'server_8.py':'9008',
'server_9.py':'9009',
'server_10.py':'9010',
'server_11.py':'9001',
'server_12.py':'9002',
'server_13.py':'9003',
'server_14.py':'9004',
'server_15.py':'9005',
'server_16.py':'9006',
'server_17.py':'9007',
'server_18.py':'9008',
'server_19.py':'9009',
'server_20.py':'9010'
}

ssh_mapping = {
'web_11.py': '2201',
'web_12.py': '2202',
'web_13.py': '2203',
'web_14.py': '2204',
'web_15.py': '2205',
'web_16.py': '2206',
'web_17.py': '2207',
'web_18.py': '2208',
'web_19.py': '2209',
'web_20.py': '2210',
'server_1.py':'2301',
'server_2.py':'2302',
'server_3.py':'2303',
'server_4.py':'2304',
'server_5.py':'2305',
'server_6.py':'2306',
'server_7.py':'2307',
'server_8.py':'2308',
'server_9.py':'2309',
'server_10.py':'2310',
'server_11.py':'2301',
'server_12.py':'2302',
'server_13.py':'2303',
'server_14.py':'2304',
'server_15.py':'2305',
'server_16.py':'2306',
'server_17.py':'2307',
'server_18.py':'2308',
'server_19.py':'2309',
'server_20.py':'2310'
}



